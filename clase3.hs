resta :: Integer -> Integer -> Integer
resta x y = x - y

suma :: Integer -> Integer -> Integer
suma x y = x + y

digitos :: Integer -> Integer 
digitos x = x

combinatorio :: Integer -> Integer -> Integer
combinatorio n m 
	| n == m = 1
	| m == 1 = n
	| otherwise = (combinatorio (n - 1) m) + combinatorio (n - 1) (m - 1)
	
sumaImparCuadradosMenorQue :: Integer -> Integer
sumaImparCuadradosMenorQue n = auxSumaImparCuadradosMenorQue 1 n

auxSumaImparCuadradosMenorQue :: Integer -> Integer -> Integer
auxSumaImparCuadradosMenorQue n m
	| n^2 > m = 0
	| n^2 <= m = n + auxSumaImparCuadradosMenorQue (n + 2) m
	
teoremaDelResto :: Integer -> Integer -> (Integer, Integer)
teoremaDelResto a d 
	| d == 0 = (0, 0)
	| a > 0 && d > 0 = calcularResto a d 1
	| a < 0 && d < 0 = calcularResto (-a) (-d) 1
	| a < 0 && d > 0 = ((- (fst qr')) , snd qr') 
	| a > 0 && d < 0 = ((- (fst qr')) , snd qr') 
	where qr' = calcularResto (-a) d 1
	
calcularResto :: Integer -> Integer -> Integer -> (Integer, Integer)
calcularResto a d q
	| a < d = (0, a)
	| (a - d) >= d = calcularResto (a - d) d (q + 1)
	| otherwise = (q, (a - d))
	
division :: Integer -> Integer -> ( Integer , Integer)
division a d 
	| a < d = (0 , a)
	| otherwise = ( fst qr' + 1, snd qr')
	where qr' = division (a - d) d
	
divParcial :: Integer -> Integer -> [Integer]
divParcial n m
	| m > n || m == 0 = []
	| m <= n && snd(teoremaDelResto n m) == 0 = [m] ++ divParcial n (m - 1)
	| otherwise = divParcial n (m - 1)
	
divisores :: Integer -> [Integer]
divisores n = divParcial n n

esPrimo :: Integer -> Bool
esPrimo n = length (divisores n) == 2

productoria :: [Integer] -> Integer
productoria a
	| length a == 0 = 1
	| otherwise = head a * productoria (tail a)
	
reverso :: [a] -> [a]
reverso a
	| length a == 0 = []
	| otherwise = reverso (tail a) ++ [head a]

prodInterno :: [Float] -> [Float] -> Float
prodInterno a b
	| length a == 0 = 0
	| otherwise = head a * head b + prodInterno (tail a) (tail b)
	
noTieneDivisoresHasta :: Integer -> Integer -> Bool
noTieneDivisoresHasta n m
	| m < 2 = True
	| mod n m == 0 && m >= 2 = False
	| mod n m /= 0 = noTieneDivisoresHasta n (m - 1)
	
otroPrimo :: Integer -> Bool
otroPrimo n = noTieneDivisoresHasta n (n - 1)

sumaDeListas :: [Integer] -> [Integer] -> [Integer]
sumaDeListas a b
	|length a == 0 = []
	| otherwise = (head a + head b) : sumaDeListas (tail a) (tail b)


mcd :: Integer -> Integer -> Integer
mcd a b 
    | a == 0 = b
    | b == 0 = a
    | a >= b = mcd (snd(teoremaDelResto a b)) b
    | b > a = mcd (snd(teoremaDelResto b a)) a

pertenece :: Integer -> [Integer] -> Bool
pertenece numerito lista 
    | length lista == 0 = False
    | numerito == head (lista) = True
    | otherwise = pertenece numerito (tail lista)


hayRepetidos :: [Integer] -> Bool
hayRepetidos lista
    | length lista == 0 = False
    | pertenece (head lista) (tail lista) = True
    | otherwise = hayRepetidos (tail lista)

darVueltaLista :: [Integer] -> [Integer]
darVueltaLista lista 
    | length (tail lista) == 0 = lista
    | otherwise = darVueltaListaAux lista []

darVueltaListaAux :: [Integer] -> [Integer] -> [Integer]
darVueltaListaAux lista1 lista2
    | length lista1 == 0 = lista2
    | otherwise = darVueltaListaAux (tail lista1) ((head lista1) : lista2)
--capicuaPara :: [Integer] -> [Integer]capicuaPara num = []

--capicuaPara2 :: [Integer] -> [Integer] -> Integer
--capicuaPara2 lista1 lista2 = (head lista1 + (getUltimoElementoDeLista lista1)) 
    
getUltimoElementoDeLista :: [Integer] -> Integer
getUltimoElementoDeLista lista
    | length lista == 0 = 0
    | length lista == 1 = head lista
    | otherwise = getUltimoElementoDeLista (tail lista)

menores :: Integer -> [Integer] -> [Integer]
menores num list = menoresAux num list []

menoresAux :: Integer -> [Integer] -> [Integer] -> [Integer]
menoresAux num listoriginal listaux
	| length listoriginal == 0 = listaux
	| num > (head listoriginal) = menoresAux num (tail listoriginal) (head listoriginal : listaux)
	| otherwise = menoresAux num (tail listoriginal) listaux
	
quitar :: Integer -> [Integer] -> [Integer]
quitar num list = quitarAux num list []

quitarAux :: Integer -> [Integer] -> [Integer] -> [Integer]
quitarAux num listoriginal listaux
	| length listoriginal == 0 = listaux
	| num == (head listoriginal) =  listaux ++ (tail listoriginal) 
	| otherwise = quitarAux num (tail listoriginal) (listaux ++ [head listoriginal])

maxEntreDos :: Integer -> Integer -> Integer
maxEntreDos m l 
	|	m>l = m
	|	l>m  = l 
	|	otherwise = m 

maximo :: [Integer] -> Integer
maximo lista
	| length lista == 2 = maxEntreDos (head lista) (head (tail lista))
	| head lista >= head (tail lista) = maximo (head lista : (tail (tail lista)))
	| otherwise = maximo (tail lista)
	
enBase :: Integer -> Integer -> [Integer]
enBase numero base = enBaseAux numero base []

enBaseAux :: Integer -> Integer -> [Integer] -> [Integer]
enBaseAux numaux base listaux
	| (fst(division numaux base)) == 0 = [(snd(division numaux base))] ++ listaux 
	| otherwise = enBaseAux (fst(division numaux base)) base ((snd(division numaux base)) : listaux)
	
deBase :: Integer -> [Integer] -> Integer
deBase base numero = deBaseAux base numero 0

getListLength :: [Integer] -> Integer -> Integer
getListLength lista largo
	| length lista == 0 = largo
	| otherwise = getListLength (tail lista) largo + 1
	
deBaseAux :: Integer -> [Integer] -> Integer -> Integer
deBaseAux base numero numeroaux
	| length numero == 1 = numeroaux + (head numero)
	| otherwise = deBaseAux base (tail numero) (numeroaux +  (head numero * base^(getListLength (tail numero) 0)))
	
esMultiploDe :: Integer -> Integer -> Bool
esMultiploDe nro multiplo
	| snd(division multiplo nro) == 0 = True
	| otherwise = False
	
intercalar :: [a] -> [a] -> [a]
intercalar lista1 lista2 = intercalarAux lista1 lista2 

intercalarAux :: [a] -> [a] -> [a]
intercalarAux lista1 lista2 
	| length lista1 == 0 = []
	| otherwise = (head lista1) : (head lista2) : intercalarAux (tail lista1) (tail lista2) 
	
quitarTodosLos :: Integer -> [Integer] -> [Integer]
quitarTodosLos x lista = quitarTodosAux x lista 

quitarTodosAux :: Integer -> [Integer] -> [Integer]
quitarTodosAux num listoriginal 
	| length listoriginal == 0 = []
	| num == (head listoriginal) =  quitarTodosAux num (tail listoriginal) 
	| otherwise = (head listoriginal) : quitarTodosAux num (tail listoriginal)

comprimo :: [Integer] -> [(Integer, Integer)]
comprimo lista = comprimoAux [] (convertirEnTuplas lista)

convertirEnTuplas :: [Integer]  -> [(Integer, Integer)]
convertirEnTuplas lista 
	| length lista == 0 = []
	| otherwise = (head lista, 1) : convertirEnTuplas (tail lista)
	
comprimoAux :: [(Integer, Integer)] -> [(Integer, Integer)] -> [(Integer, Integer)]
comprimoAux lista listaux
	| length listaux == 0 = reverso lista
	| length lista == 0 = comprimoAux [(head listaux)] (tail listaux)
	| fst(head lista) == fst(head listaux) = comprimoAux ((fst(head lista), snd(head lista) + 1) : (tail lista)) (tail listaux)
	| otherwise = comprimoAux ((head listaux) : lista) (tail listaux)

	
capicuaPara :: [Integer] -> [Integer] 
capicuaPara numero
	| reverso numero == numero = numero
	| otherwise = capicuaPara (enBase ((deBase 10 numero) + (deBase 10 (reverso numero))) 10)

calculosMcd :: Integer -> Integer -> (Integer, Integer)
calculosMcd a b = calculosMcdAux a b (a, 1)


calculosMcdAux :: Integer -> Integer -> (Integer, Integer) -> (Integer, Integer)
calculosMcdAux a b divisor
	| (((mod a (fst(divisor))) == 0) && ((mod b (fst divisor)) == 0)) = divisor
	| otherwise = calculosMcdAux a b (fst(divisor) - 1, snd(divisor) + 1)
	
ordenarLista :: [Integer] -> [Integer]
ordenarLista lista
	| chequeoOrdenamiento lista == False = ordenarLista (ordenarListaAux [] lista)
	| otherwise = lista
	
chequeoOrdenamiento :: [Integer] -> Bool
chequeoOrdenamiento lista 
	| length lista == 1 = True
	| (head lista) > (head (tail lista)) = False
	| otherwise = chequeoOrdenamiento (tail lista)
	
ordenarListaAux :: [Integer] -> [Integer] -> [Integer]
ordenarListaAux lista listaux
	| length listaux == 1 = lista ++ listaux
	| (head listaux) > (head (tail listaux)) = ordenarListaAux (lista ++ [(head (tail listaux))]) ((head listaux) : (tail (tail listaux)))
	| otherwise = ordenarListaAux (lista ++ [(head listaux)]) (tail listaux)

minimo :: [Integer] -> Integer
minimo lista = minimoAux (head lista) lista

minimoAux :: Integer -> [Integer] -> Integer
minimoAux nro lista
	| length lista == 0 = nro
	| nro < (head lista) = minimoAux nro (tail lista)
	| otherwise = minimoAux (head lista) (tail lista)

eliminarRepetidosUlt :: [Integer] -> [Integer]
eliminarRepetidosUlt lista = eliminarRepetidosUltAux lista

eliminarRepetidosUltAux :: [Integer] -> [Integer]
eliminarRepetidosUltAux lista 
	| length lista == 0 = lista
	| estaRepetidoUltElemento (head lista) (tail lista) = eliminarRepetidosUltAux (tail lista)
	| otherwise = (head lista) : eliminarRepetidosUltAux (tail lista)
	
estaRepetidoUltElemento :: Integer -> [Integer] -> Bool
estaRepetidoUltElemento num lista
	| length lista == 0 = False
	| (head lista) == num = True
	| otherwise = estaRepetidoUltElemento num (tail lista)

sumaAcumulada :: [Integer] -> [Integer]
sumaAcumulada lista = sumaAcumuladaAux lista []

sumaAcumuladaAux :: [Integer] -> [Integer] -> [Integer]
sumaAcumuladaAux lista listaux
	|	length lista == 0 = reverso listaux
	|	length listaux == 0 = sumaAcumuladaAux (tail lista) [(head lista)]
	|	otherwise =  sumaAcumuladaAux (tail lista) ((head listaux + (head lista)) : listaux )
	
sumarPares :: [Integer] -> Integer
sumarPares lista
	| length lista == 0 = 0
	| mod (head lista) 2 == 0 = (head lista) + sumarPares (tail lista)
	| otherwise = sumarPares (tail lista)

posiciones :: [Integer] -> Integer -> [Integer]
posiciones lista elemento = posicionesAux lista elemento 0

posicionesAux :: [Integer] -> Integer -> Integer -> [Integer]
posicionesAux lista elemento contador
	| length lista == 0 = []
	| (head lista) == elemento = contador : posicionesAux (tail lista) elemento (contador + 1)
	| otherwise = posicionesAux (tail lista) elemento (contador + 1)

